def getEbayPrice(book_info):

    '''
    Find the price of a book on eBay for different conditions
    '''

    from ebaysdk.finding import Connection as Finding
    from ebaysdk.exception import ConnectionError

    category_ids = set([str(x) for x in [267, 45110, 29223, 29792, 118254, 182882, 171210, 171222, 279, 1093, 11104, 171228, 280, 171243, 2228, 29399, 268]])

    try:
        api = Finding(appid="LisaMari-Bookpric-PRD-451ca6568-8cfd58f3", config_file=None)
        response = api.execute('findItemsAdvanced', {'keywords': book_info})
        results = response.dict()['searchResult']['item']
        item_id = [x['itemId'] for x in results]
        title = [x['title'] for x in results]
        condition = [x['condition']['conditionDisplayName'] for x in results]
        price = [x['sellingStatus']['currentPrice']['_currencyId'] + x['sellingStatus']['currentPrice']['value'] for x in results]
        if results[0]['primaryCategory']['categoryId'] in category_ids:
            return title, condition, price, item_id
        else:
            return None
    except ConnectionError as e:
        print(e)
        print(e.response.dict())

def ebayRanges(info):
    condition_dict = {}
    for condition in set(info[1]):
        condition_dict[condition] = min([x for i,x in enumerate(info[2]) if info[1][i] == condition])
    return condition_dict


def findNames(text):

    '''
    Identify names in a string of text
    '''
    
    import nltk
    from nameparser.parser import HumanName

    tokens = nltk.tokenize.word_tokenize(text)
    pos = nltk.pos_tag(tokens)
    sentt = nltk.ne_chunk(pos, binary = False)
    person_list = []
    person = []
    name = ""
    
    for subtree in sentt.subtrees():
        if subtree.label() == 'PERSON':
            for leaf in subtree.leaves():
                 person.append(leaf[0])
            for part in person:
                name += part + ' '
            if name[:-1] not in person_list:
                person_list.append(name[:-1])
            name = ''
            person = []

    return person_list

def spellCheck(word):

    '''
    Spell check words that aren't in the nltk corpus
    '''

    import enchant
    d = enchant.Dict("en_US")

    if not d.check(word):
        return d.suggest(word)[0]
    else:
        return word

def cleanText(txt):

    '''
    Clean text pulled off book images
    '''

    import nltk
    import string
    
    text = ''
    for char in txt:
        if char in string.punctuation:
            text += ' '
        else:
            text += char

    #translator = str.maketrans('', '', string.punctuation)
    words = set(nltk.corpus.words.words())
    words.add('mysql')
    words.add('sql')
    words.add('coffeescript')
    words.add('programming')
    words.add('javascript')
    words.add('systems')
    words.add('conversions')
    words.add('html')
    words.add('css')
    
    # remove punctuation, '\n', uppercase, non-english words and digits
    cleaned = text.replace('\n', ' ') 
    cleaned = cleaned.replace('0', 'o')
    cleaned = cleaned.replace('1', 'l')
    cleaned = cleaned.lower()
    #cleaned = cleaned.translate(translator)
    cleaned = ' '.join([word for word in cleaned.split() if len(word) > 1 and not word.isdigit()]) # remove words of length < 2 and digits
    #cleaned = ' '.join([spellCheck(word) if word not in words else word for word in cleaned.split()])
    cleaned = ' '.join(['' if word not in words else word for word in cleaned.split()])
    names = findNames(txt)
    input_text = cleaned.lower() #+ ' ' + ' '.join([name.lower() for name in names if name.lower() not in set(cleaned.lower().split())])
    return input_text

def separateBooks(img, n_books):

    '''
    Separates multiple books using Kmeans clustering
    '''

    # import the necessary packages
    from sklearn.cluster import KMeans
    import matplotlib.pyplot as plt
    import argparse
    import utils
    import cv2
    import numpy as np
    import imutils
    from resizeimage import resizeimage
    from PIL import Image
    import math

    n_books = int(n_books)
    
    def centroid_histogram(clt):
        # grab the number of different clusters and create a histogram
        # based on the number of pixels assigned to each cluster
        numLabels = np.arange(0, len(np.unique(clt.labels_)) + 1)
        (hist, _) = np.histogram(clt.labels_, bins = numLabels)

        # normalize the histogram, such that it sums to one
        hist = hist.astype("float")
        hist /= hist.sum()

        # return the histogram
        return hist
    
    flat = np.array(Image.open(img))

    # rotate and resize image
    if flat.shape[2] == 4:
        png = Image.fromarray(flat, 'RGBA')
        jpg = png.convert('RGB')
        rot = jpg.rotate(90, expand=True)
        flat = np.array(jpg)
    else:
        rot = Image.fromarray(flat, 'RGB').rotate(90, expand=True)

    tol = 10
    fill_tol = 37
    total_books = 0
    book_coords = []
    failed_times = 0
    for i in range(0, 6):
        
        if total_books == n_books:
            break
        
        if i == 0:
            next_im = np.array(rot.copy())

        cover = resizeimage.resize_cover(Image.fromarray(next_im), [500, 500], validate=False)

        flat_crop = np.array(cover)

        # reshape the image to be a list of pixels
        image = flat_crop.reshape((flat_crop.shape[0] * flat_crop.shape[1], 3))
        
        clt = KMeans(n_clusters = (n_books - total_books)*2)
        clt.fit(image)

        # build a histogram of clusters and then create a figure
        # representing the number of pixels labeled to each color
        hist = centroid_histogram(clt)
        
        current_found = 0

        for target_color in clt.cluster_centers_:

            # skip pure white
            if not all([math.ceil(x) == 255 for x in target_color]):

                # find color mask

                lower = np.array([x - tol if x >= tol else 0 for x in target_color], np.uint8)       # HSV color code lower and upper bounds
                upper = np.array([x + tol if x <= 255-tol else 255 for x in target_color], np.uint8)       # color yellow 

                mask = cv2.inRange(next_im, lower, upper)
                output = cv2.bitwise_and(next_im, next_im, mask = mask)


                # image preprocessing (greyscale and blur)
                gray = cv2.cvtColor(output, cv2.COLOR_BGR2GRAY)
                gray = cv2.GaussianBlur(gray, (5, 5), 0)
                cv2.imwrite('python_book_gray.jpg', gray)

                # edge detection
                edged = cv2.Canny(gray,1, 7)

                # close boundaries 
                kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (fill_tol, fill_tol))
                closed = cv2.morphologyEx(edged, cv2.MORPH_CLOSE, kernel)
        
                # find shapes
                _, contours, _= cv2.findContours(closed, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

                # determine if shapes are books

                for c in contours:
                    # approximate the contour
                    peri = cv2.arcLength(c, True)
                    approx = cv2.approxPolyDP(c, 0.02 * peri, True)
                                
                # if the approximated contour has four points, then assume that the
                # contour is a book -- a book is a rectangle and thus has four vertices
                    if len(approx) == 4:
                        
                        area = cv2.contourArea(c)
                        hull = cv2.convexHull(c)
                        hull_area = cv2.contourArea(hull)
                        solidity = float(area)/hull_area
                        
                        
                        perimeter = cv2.arcLength(c,True)
                        if perimeter > 4000 and solidity > 0.5:

                            fill_coords = approx.copy()
                            # extend fill polygons to cover more color
                            for i in range(0, 4):
                                if fill_coords[i][0][0] < next_im.shape[1] / 2:
                                    fill_coords[i][0][0] = 0
                                else:
                                    fill_coords[i][0][0] = next_im.shape[1]

                            midpoint = np.mean([x[0][1] for x in approx]) 

                            for i in range(0, 4):
                                if fill_coords[i][0][1] < midpoint:
                                    fill_coords[i][0][1] -= 40
                                else:
                                    fill_coords[i][0][1] += 40

                            cv2.fillPoly(next_im, [fill_coords], (255, 255, 255))
                            book_coords.append(approx)
                            total_books += 1
                            current_found += 1
                            if total_books == n_books:
                                break

        if current_found == 0:
            failed_times += 1
            if failed_times == 1:
                tol = -5
            else:
                tol += 10


    # separate books into separate images
    individual_books = []
    rot = np.array(rot.copy())
    def separateBooks(coordinates):

        for book in range(0, len(coordinates)):
            min_y = min([x[0][1] for x in coordinates[book]])
            max_y = max([x[0][1] for x in coordinates[book]])
            cropped_book = rot[min_y:max_y,]
            individual_books.append(Image.fromarray(cropped_book))
    separateBooks(book_coords)
    return individual_books, book_coords


def fixSkew(which_book, coords):

    '''
    Function to calculate the skew of a book and how many pixels it will be rotated by
    '''

    import math
    
    corners = sorted([x[:,1][0] for x in coords[which_book]])
    op = (abs(corners[0] - corners[1]) + abs(corners[2] - corners[3])) / 2

    corners = sorted([x[:,0][0] for x in coords[which_book]])
    adj = abs((corners[0] + corners[1]) / 2 - (corners[2] + corners[3]) / 2)
    
    angle = math.degrees(math.tan(op/adj))
    
    coord = [[x[0][0], x[0][1]] for x in coords[which_book]]
    coord.sort()
    if coord[0][1] + coord[1][1] > coord[2][1] + coord[3][1]:
        return -angle, op
    else:
        return angle, op

def rotate(image, angle, color):

    '''
    Rotates book image given the calculated angle
    '''

    from PIL import Image
    
    bg = Image.new("RGB", image.size, color)
    im = image.convert("RGBA").rotate(angle)
    bg.paste(im, im)
    return bg

def rotateBooks(book_list, coords):
    import math
    import pyocr
    
    tools = pyocr.get_available_tools()[1]
    rotated_books = []
    for i, book in enumerate(book_list):
        angle, op = fixSkew(i, coords)
        if abs(angle) >= 1:
            rotated = rotate(book, angle, 'white')
            width = book.size[0]
            height = book.size[1]
            cropped = rotated.crop((0, op-80, width, height-op+80))
            rotated_books.append(cropped)
        else:
            rotated_books.append(book)
    return rotated_books
