def textFromImage(img):

    import cv2
    import numpy as np
    import pandas as pd
    import pytesseract
    from PIL import Image, ImageOps


    img = cv2.imread(img)
    grayscale = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    gray = cv2.GaussianBlur(grayscale, (5, 5), 0)
    
    im = Image.fromarray(gray)
    gray = im.convert('L')
    bw = gray.point(lambda x: 0 if x<128 else 255, '1')
    
    inverted_image = ImageOps.invert(bw.convert('L'))
    
    text = pytesseract.image_to_string(inverted_image)
    
    return text

def textFromMultipleImage(rotated_books, individual_books):

    import tesserocr as tr
    import cv2
    import numpy as np
    from PIL import Image

    clean_text = []
    for i, book in enumerate(rotated_books):

        img = book
        book_text = ''

        large = img.resize((img.size[0]*2, img.size[1]*2))
        grayscale = cv2.cvtColor(np.array(large), cv2.COLOR_BGR2GRAY)
        gray_np = cv2.GaussianBlur(grayscale, (3, 3), 0)

        found_text = True
        threshold = -5
        allowed_tries = 10
        linesize = 5

        while found_text:

            im = Image.fromarray(gray_np)
            gray = im.convert('L')

            original_gray = cv2.cvtColor(np.array(individual_books[i]), cv2.COLOR_BGR2GRAY)

            ret, otsu = cv2.threshold(np.array(original_gray) , 0, 255, cv2.THRESH_BINARY+cv2.THRESH_OTSU)
            bw = gray.point(lambda x: 255 if x<ret+threshold else 0, '1')

            cv_img = np.array(bw)
            color_image = np.array(large)

            #initialize api
            api = tr.PyTessBaseAPI()

            api.SetVariable("textord_min_linesize", str(linesize))
            api.SetVariable("tessedit_char_blacklist", "/+='(){}-%$^&^.\,_|")
            try:
                # set pil image for ocr
                api.SetImage(bw)
                boxes = api.GetComponentImages(tr.RIL.TEXTLINE,True)
                # get text
                text = api.GetUTF8Text()
                #book_text += ' ' + text
                gray_np = np.array(gray)
                # iterate over returned list, draw rectangles
                if len(boxes) == 0:
                    allowed_tries -= 1
                    if allowed_tries == 0:
                        found_text = False
                else:
                    for word, confidence in zip(api.AllWords(), api.AllWordConfidences()):
                        if word != ' ' and confidence > 70:
                            book_text += ' ' + word
                    for (im,box,_,_) in boxes:
                        x,y,w,h = box['x'],box['y'],box['w'],box['h']
                        cv2.rectangle(color_image, (x,y), (x+w,y+h), color=(0,0,255), thickness=10)
                        cv2.rectangle(gray_np, (x,y), (x+w,y+h), color=(0,0,0), thickness=-1)
                        crop_size = (x, y, x + w, y + h)
                        cropped = bw.crop(crop_size)

            finally:
                api.End()

            threshold += 5
            linesize -= 0.5

        clean_text.append(book_text)
    return clean_text