from flask import render_template
from flask import request
from flaskexample import app
from flaskexample.text_from_image import textFromImage, textFromMultipleImage
from flaskexample.ebay_price import getEbayPrice, ebayRanges, cleanText, separateBooks, fixSkew, rotateBooks, rotate
import pickle


@app.route('/')
def home():
    return render_template("home_page.html")

@app.route('/about-page')
def about():
    return render_template("about.html")

@app.route('/slides-page')
def slides():
    return render_template("slides.html")

@app.route('/contact-info')
def contact():
    return render_template("contact.html")

@app.route('/single-input')
def single_book_input():
    return render_template("book_pricer_single.html")

@app.route('/multiple-input')
def multiple_book_input():
    return render_template("book_pricer_multiple.html")

@app.route('/single-upload', methods=['POST'])
def single_book_upload():
    if request.method == 'POST':

        if request.files:
            file = request.files['image']
            file.save(app.root_path + '/static/img/' + file.filename)
            filename = '/static/img/' + file.filename
            
        else:
            if 'example_1' in request.form:
                filename = '/static/img/marketing_book.jpg'
            else:
                filename = '/static/img/fiction_example.jpg'


        title = cleanText(textFromImage(app.root_path + filename))

        try:
            book_info = getEbayPrice(cleanText(title))
            ebay_title = book_info[0][0]
            table_data = ebayRanges(book_info)
            table_data['Title'] = ebay_title
            all_prices = [table_data]
            outcome = "On eBay that book is listed for:"
        except:
            all_prices = [{}]
            outcome = "We couldn't find that book on ebay"


    return render_template("output.html", user_image = filename, ebay_price = all_prices, intro = outcome, missed = [])

@app.route('/multiple-upload', methods=['POST'])
def multiple_book_upload():
    if request.method == 'POST':
        if request.files:

            file = request.files['image']
            file.save(app.root_path + '/static/img/' + file.filename)
            filename = '/static/img/' + file.filename

            n_books = request.form['n_books']
            
        else:
            if 'example_1' in request.form:
                filename = '/static/img/three_books.jpg'
                n_books = 3
            else:
                filename = '/static/img/bookshelf_small.jpg'
                n_books = 7                

        
        individual_books, book_coords = separateBooks(app.root_path + filename, n_books)
        rotated_books = rotateBooks(individual_books, book_coords)
        text = textFromMultipleImage(rotated_books, individual_books)


        import os

        dir_name = app.root_path + '/static/img/'
        test = os.listdir(dir_name)

        for item in test:
            if item.startswith("missed_books"):
                os.remove(os.path.join(dir_name, item))


        book_list = []
        missed_books = []
        for i,t in enumerate(text):
            print(t)
            try:
                book_info = getEbayPrice(cleanText(t))
                ebay_title = book_info[0][0]
                table_data = ebayRanges(book_info)
                table_data['Title'] = ebay_title
                all_prices = [table_data]
                book_list.append(table_data)
            except:
                print('Couldnt find book' + str(i))
                rotated_books[i].save(app.root_path + '/static/img/missed_books' + str(i) + '.jpg')
                print('../static/img/missed_books' + str(i) + '.jpg')
                missed_books.append({'url': '../static/img/missed_books' + str(i) + '.jpg'})

        if len(book_list) == 0:
            book_list.append({})

        if len(book_list) == 0:
            outcome = "We couldn't find those books on ebay"
        else:
            outcome = "Here are the books we found on ebay:"

        with open(app.root_path + '/static/book_list.pkl', 'wb') as f:
            pickle.dump(book_list, f)

        return render_template("output.html", user_image = filename, ebay_price = book_list, intro = outcome, missed = missed_books)

@app.route('/multiple-upload-secondary', methods=['POST'])
def multiple_book_upload_secondary():
    if request.method == 'POST':

        titles = request.form.getlist('true_title')
        filename = request.form['previous_image']

        print(titles)
        
        with open(app.root_path + '/static/book_list.pkl', 'rb') as f:
            old_books = pickle.load(f)

        for t in titles:
            try:
                book_info = getEbayPrice(cleanText(t))
                ebay_title = book_info[0][0]
                table_data = ebayRanges(book_info)
                table_data['Title'] = ebay_title
                all_prices = [table_data]
                old_books.append(table_data)
            except:
                print('Couldn\'t find that book')

        outcome = "Here are the books we found on ebay:"

        return render_template("output.html", user_image = filename, ebay_price = old_books, intro = outcome, missed = [])