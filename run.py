#!/usr/bin/env python

import sys
from flaskexample import app

if len(sys.argv) >= 2 and sys.argv[1] == 'prod':
    print('Detected production mode')
    app.run()
else:
    print('Detected debug mode')
    app.run(debug = True)
